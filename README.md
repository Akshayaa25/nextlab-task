# NextLab Task

#Problem Set-1
Please refer file - "Task 1.py"



#Problem Set-2

# Project Overview

The Django project facilitates interaction between an admin user and regular users. It consists of an Admin Facing section for managing Android apps and points, and a User Facing section for users to view app listings, track points, and submit task completion evidence through screenshots.

## Prerequisites

Before running the project, ensure you have the following prerequisites installed:

- Python (3.6 or higher)
- Django
- Django REST Framework
- Pillow
- MySQL

## Installation

Follow these steps to set up and run the project:

1. Clone the repository or download the project files.

2. Configure the MySQL Database:
   - Install and configure MySQL on your system.
   - Create a new MySQL database named "webapp".
   - Open the project's settings file (`settings.py`) and update the `DATABASES` configuration with your MySQL database details.

3. Apply Migrations:
   ```
   python manage.py migrate
   ```
4. Run the Development Server:
   ```
   python manage.py runserver
   ```
5. Open a web browser and navigate to `http://127.0.0.1:8000/` to access the project.

**API Endpoints:**
- GET /api/list/admin/ - Retrieve a list of all admin users.
- GET /api/list/user/ - Retrieve a list of all regular users.
- POST /api/create/admin/ - Create a new admin user.
- POST /api/create/user/ - Create a new regular user.
- POST /api/create/app/ - Create a new Android app.
- GET /api/list/app/ - Retrieve a list of all Android apps.
- GET /api/tasks/completed/<str:username>/ - Retrieve a list of completed tasks by a specific user.
- GET /api/task/details/<int:id>/ - Retrieve details of a specific task (Android app).



#Problem Set-3

**A. Write and share a small note about your choice of system to schedule periodic tasks (such as downloading a list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?**
I have opted for a cron-based task scheduler for scheduling periodic tasks because it is reliable and easy to use. However, if we anticipate a significant increase in the number of tasks or complexity, cron may become difficult to manage. In such cases, I suggest considering more advanced task scheduling systems like Apache Airflow, Celery, or Kubernetes CronJobs, which offer enhanced scalability and better management capabilities in production environments.

**B.In what circumstances would you use Flask instead of Django and vice versa?**
Use Flask when you want a lightweight framework for small projects, need maximum flexibility and customization, prefer a microservices architecture, or require a shallow learning curve for quick prototyping.
Use Django when you need a full-featured framework for complex web applications, want rapid development with built-in functionalities like ORM, admin interface, and authentication, are building content-driven websites with extensive database integration, or require scalability and security features for high-traffic applications.
